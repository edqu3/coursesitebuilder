/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.data;

/**
 * Simple struct used to store link content, id, etc.
 * @author edqu3
 */
public class PageLinkProperties {
    public final String LINK_ID;
    public final String LINK_HEADER;        
    public final String LINK_PAGE;
    public final String LINK_CLASS;
    public PageLinkProperties(String link_id, String link_header, String link_page, String link_class) {
        this.LINK_ID     = link_id;
        this.LINK_HEADER = link_header;
        this.LINK_PAGE   = link_page;
        this.LINK_CLASS  = link_class;
    }
}
